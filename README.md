ReactNative Example
===
This is an example application for pumpup.

#### Technology stack
* React/Redux
* ReduxThunk/Axios
* NodeJS/NPM/Yarn
* Mocha/Should/Sinon/Enzyme/Proxyquire/eslint

#### Features
* React text parser for mention and hashtag
* Populate application with data from pumpup
* Image swiper with buttons
* Grid of photos


#### Running
1. `npm install` or `npm install`
2. `npm start` or `yarn start`

#### Testing
1. `npm test`

#### Known issues
* Swiper component `react-native-swiper` library doesn't call callback `onIndexChanged`.
So, when you swipe, the dot items don't update.


### Screenshots
![](https://i.imgur.com/mRHPblm.png)
![](https://i.imgur.com/1Oao7Zv.png)
![](https://i.imgur.com/FY6ct0x.png)
![](https://i.imgur.com/AmtDk8m.png)
