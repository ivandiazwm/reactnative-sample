import React from 'react'
import {Text, StyleSheet} from 'react-native'

const PARSING_TEXT    = 0
const PARSING_HASHTAG = 1
const PARSING_MENTION = 2

class RichTextParser {

  parse(text) {

    let renderList     = []
    let parsingLink    = false
    let parsingType    = PARSING_TEXT
    let parsingSegment = ''

    // Time Complexity: O(n), n = text.length
    for(let index = 0; index < text.length; ++index) {
      let character = text[index]

      if(character === '#' || character === '@') {
        renderList.push(this.compileSegment(parsingSegment, parsingType, renderList.length))

        parsingLink    = true
        parsingType    = (character === '#') ? PARSING_HASHTAG : PARSING_MENTION
        parsingSegment = ''
      } else if(!this.isAlphanumeric(character) && parsingLink) {
        renderList.push(this.compileSegment(parsingSegment, parsingType, renderList.length))

        parsingLink    = false
        parsingType    = PARSING_TEXT
        parsingSegment = character
      } else {
        parsingSegment += character
      }
    }

    renderList.push(this.compileSegment(parsingSegment, parsingType, renderList.length))

    return renderList

  }



  isAlphanumeric(string) {

    return /[a-zA-Z0-9]/.test(string)

  }



  compileSegment(segment, parsingType, index) {

    switch(parsingType) {
      case PARSING_HASHTAG:
      return (
        <Text key={index} style={styles.link} onPress={this.onHashtagClicked.bind(this, segment)}>
            #{segment}
        </Text>
      )
      case PARSING_MENTION:
        return (
          <Text key={index} style={styles.link} onPress={this.onMentionClicked.bind(this, segment)}>
              @{segment}
          </Text>
        )
      default:
        return (
          <Text key={index}>
            {segment}
          </Text>
        )
    }

  }



  onHashtagClicked(text) {

    alert('clicked hashtag #' + text)

  }



  onMentionClicked(text) {

    alert('clicked mention @' + text)

  }

}

export default new RichTextParser()


const styles = StyleSheet.create({
  link: {
    color: 'blue'
  }
})
