import {Text} from 'react-native'
import richTextParser from '../rich-text-parser.js'

describe('RichTextParser', () => {

  let should = global.should
  let sinon = global.sinon

  let clickItemAndAssert

  before(() => {

    richTextParser.onHashtagClicked = sinon.stub()
    richTextParser.onMentionClicked = sinon.stub()

    clickItemAndAssert = (item) => {
      richTextParser.onHashtagClicked.reset()
      richTextParser.onMentionClicked.reset()

      item.props.onPress()

      if(item.props.children[0] === '@') {
        richTextParser.onMentionClicked.should.be.calledOnce()
        richTextParser.onMentionClicked.should.be.calledWith(item.props.children[1])
      } else {
        richTextParser.onHashtagClicked.should.be.calledOnce()
        richTextParser.onHashtagClicked.should.be.calledWith(item.props.children[1])
      }
    }
  })

  it('should check alphanumeric characters', () => {

    should(richTextParser.isAlphanumeric('a')).be.equal(true)
    should(richTextParser.isAlphanumeric('q')).be.equal(true)
    should(richTextParser.isAlphanumeric('w')).be.equal(true)
    should(richTextParser.isAlphanumeric('1')).be.equal(true)
    should(richTextParser.isAlphanumeric('5')).be.equal(true)
    should(richTextParser.isAlphanumeric('.')).be.equal(false)
    should(richTextParser.isAlphanumeric('\n')).be.equal(false)
    should(richTextParser.isAlphanumeric(' ')).be.equal(false)
    should(richTextParser.isAlphanumeric('@')).be.equal(false)
    should(richTextParser.isAlphanumeric('#')).be.equal(false)
  })

  it('should compile a segment correctly', () => {

    let compiledSegment = richTextParser.compileSegment('lorem ipsum', 0, 3)

    should(compiledSegment.type).be.equal(Text)
    should(compiledSegment.key).be.equal('3')
    should(compiledSegment.props.children).be.equal('lorem ipsum')
  })

  it('should parse regular text', () => {

    let parsedText = richTextParser.parse(
      'Lorem ipsum @dolor sit amet, #consectetur adipiscing elit.'
    )



    should(parsedText.length).be.equal(5)

    should(parsedText[0].type).be.equal(Text)
    should(parsedText[0].key).be.equal('0')
    should(parsedText[0].props.children).be.equal('Lorem ipsum ')

    should(parsedText[1].type).be.equal(Text)
    should(parsedText[1].key).be.equal('1')
    should(parsedText[1].props.style.color).be.equal('blue')
    should(parsedText[1].props.children).be.deepEqual(['@', 'dolor'])
    clickItemAndAssert(parsedText[1])

    should(parsedText[2].type).be.equal(Text)
    should(parsedText[2].key).be.equal('2')
    should(parsedText[2].props.children).be.deepEqual(' sit amet, ')

    should(parsedText[3].type).be.equal(Text)
    should(parsedText[3].key).be.equal('3')
    should(parsedText[3].props.style.color).be.equal('blue')
    should(parsedText[3].props.children).be.deepEqual(['#', 'consectetur'])
    clickItemAndAssert(parsedText[3])

    should(parsedText[4].type).be.equal(Text)
    should(parsedText[4].key).be.equal('4')
    should(parsedText[4].props.children).be.deepEqual(' adipiscing elit.')
  })

  it('should parse complex text', () => {

    let parsedText = richTextParser.parse(
      '#Lorem ipsum @dolor, @sit\n Amet, #adipiscing+#elit.'
    )



    should(parsedText.length).be.equal(11)

    should(parsedText[0].type).be.equal(Text)
    should(parsedText[0].key).be.equal('0')
    should(parsedText[0].props.children).be.equal('')

    should(parsedText[1].type).be.equal(Text)
    should(parsedText[1].key).be.equal('1')
    should(parsedText[1].props.style.color).be.equal('blue')
    should(parsedText[1].props.children).be.deepEqual(['#', 'Lorem'])
    clickItemAndAssert(parsedText[1])


    should(parsedText[2].type).be.equal(Text)
    should(parsedText[2].key).be.equal('2')
    should(parsedText[2].props.children).be.deepEqual(' ipsum ')

    should(parsedText[3].type).be.equal(Text)
    should(parsedText[3].key).be.equal('3')
    should(parsedText[3].props.style.color).be.equal('blue')
    should(parsedText[3].props.children).be.deepEqual(['@', 'dolor'])
    clickItemAndAssert(parsedText[3])

    should(parsedText[4].type).be.equal(Text)
    should(parsedText[4].key).be.equal('4')
    should(parsedText[4].props.children).be.deepEqual(', ')

    should(parsedText[5].type).be.equal(Text)
    should(parsedText[5].key).be.equal('5')
    should(parsedText[5].props.style.color).be.equal('blue')
    should(parsedText[5].props.children).be.deepEqual(['@', 'sit'])
    clickItemAndAssert(parsedText[5])

    should(parsedText[6].type).be.equal(Text)
    should(parsedText[6].key).be.equal('6')
    should(parsedText[6].props.children).be.deepEqual('\n Amet, ')

    should(parsedText[7].type).be.equal(Text)
    should(parsedText[7].key).be.equal('7')
    should(parsedText[7].props.style.color).be.equal('blue')
    should(parsedText[7].props.children).be.deepEqual(['#', 'adipiscing'])
    clickItemAndAssert(parsedText[7])

    should(parsedText[8].type).be.equal(Text)
    should(parsedText[8].key).be.equal('8')
    should(parsedText[8].props.children).be.deepEqual('+')

    should(parsedText[9].type).be.equal(Text)
    should(parsedText[9].key).be.equal('9')
    should(parsedText[9].props.style.color).be.equal('blue')
    should(parsedText[9].props.children).be.deepEqual(['#', 'elit'])
    clickItemAndAssert(parsedText[9])

    should(parsedText[10].type).be.equal(Text)
    should(parsedText[10].key).be.equal('10')
    should(parsedText[10].props.children).be.deepEqual('.')
  })

  it('should be executed fast', () => {

    let longText = ''
    let repeat = 100

    while(repeat--) {
      longText += '#loremipsumloremipsumloremipsumloremipsumloremipsum'
      longText += ' loremipsumloremipsumloremipsumloremipsumloremipsum'
    }

    // It should be O(n), with n = 5e6
    repeat = 100
    while(repeat--) {
      richTextParser.parse(longText)
    }
  }).timeout(10000)
})
