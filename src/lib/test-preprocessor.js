global.sinon = require('sinon')
global.React = require('react')
global.Enzyme = require('enzyme');
global.shallow = Enzyme.shallow;

const Adapter = require('enzyme-adapter-react-16');
Enzyme.configure({ adapter: new Adapter() });

require('should-sinon')
require('react-native-mock-render/mock')

let proxyquireStrict = require('proxyquire').noCallThru();

global.proxyquire = function (path, mocks) {
    return proxyquireStrict(process.cwd() + '/src/' + path, mocks)
};
