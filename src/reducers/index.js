import { combineReducers } from 'redux'
import contentReducer from './content-reducer'

const rootReducer = combineReducers({
  content: contentReducer
})

export default rootReducer
