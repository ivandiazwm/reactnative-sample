import _  from 'lodash'
import { RETRIEVE_PROFILE, RETRIEVE_SLIDER, RETRIEVE_GRID } from '../actions/content-actions'

const initialState = {
  bio: '',
  name: '',
  profileThumbnail: '',
  sliderImages: [],
  gridImages: []
}

export default function (state = initialState, action) {

  switch (action.type) {
    case RETRIEVE_PROFILE:
      return _.merge({}, state, {
        bio             : action.bio,
        name            : action.name,
        profileThumbnail: action.profileThumbnail,
      })

    case RETRIEVE_SLIDER:
      return _.merge({}, state, {
        sliderImages: action.sliderImages
      })

    case RETRIEVE_GRID:
      return _.merge({}, state, {
        gridImages: action.gridImages
      })

    default:
      return state
  }

}
