describe('Content reducer', () => {

  let should     = global.should
  let proxyquire = global.proxyquire

  let contentReducer
  let initialState

  before(() => {

    initialState = {
      bio             : '',
      name            : '',
      profileThumbnail: '',
      sliderImages    : [],
      gridImages      : []
    }

    contentReducer = proxyquire('reducers/content-reducer.js', {
      '../actions/content-actions': {
        RETRIEVE_PROFILE: 'RETRIEVE_PROFILE',
        RETRIEVE_SLIDER : 'RETRIEVE_SLIDER',
        RETRIEVE_GRID   : 'RETRIEVE_GRID'
      }
    }).default

  })


  it('should return initialState if no action or state', () => {

    should(contentReducer(undefined, {})).be.deepEqual(initialState)

  })


  it('should return updated state on RETRIEVE_PROFILE', () => {

    should(contentReducer(initialState, {
      type            : 'RETRIEVE_PROFILE',
      bio             : 'MOCK_BIO',
      name            : 'MOCK_NAME',
      profileThumbnail: 'MOCK_THUMBNAIL'
    })).be.deepEqual({
      bio             : 'MOCK_BIO',
      name            : 'MOCK_NAME',
      profileThumbnail: 'MOCK_THUMBNAIL',
      sliderImages    : [],
      gridImages      : []
    })

  })


  it('should return updated state on RETRIEVE_SLIDER', () => {

    should(contentReducer(initialState, {
      type: 'RETRIEVE_SLIDER',
      sliderImages: ['IMG_1', 'IMG_2']
    })).be.deepEqual({
      bio             : '',
      name            : '',
      profileThumbnail: '',
      sliderImages    : ['IMG_1', 'IMG_2'],
      gridImages      : []
    })

  })


  it('should return updated state on RETRIEVE_GRID', () => {

    should(contentReducer(initialState, {
      type: 'RETRIEVE_GRID',
      gridImages: ['IMG_1', 'IMG_2']
    })).be.deepEqual({
      bio             : '',
      name            : '',
      profileThumbnail: '',
      sliderImages    : [],
      gridImages      : ['IMG_1', 'IMG_2']
    })

  })

})
