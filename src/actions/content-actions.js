import axios from 'axios'

//CONSTANTS
export const RETRIEVE_PROFILE = 'RETRIEVE_PROFILE'
export const RETRIEVE_SLIDER = 'RETRIEVE_SLIDER'
export const RETRIEVE_GRID = 'RETRIEVE_GRID'

//ACTIONS
export const retrieveProfile = () => {

  return (dispatch) => {

    return axios.post('http://api.pumpup.com/1/classes/User/318381', {
      '_method': 'GET',
      '_version': '5.0.5',
      '_SessionToken':
        [
          'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.',
          'eyJpc3MiOjI3MDc3OTgsImV4cCI6MTUzOTUzNTI1OTM2OH0.',
          'UK2qP1yk9QLk_Bkx1Ly0RPaitRYtec8ojZhzYRc0D-g'
        ].join('')
    })
      .then(({data}) => dispatch({
        type            : RETRIEVE_PROFILE,
        bio             : data.bio,
        name            : data.name,
        profileThumbnail: data.profileThumbnail
      }))

  }

}

export const retrieveSliderImages = () => {

  return (dispatch) => {

    return axios.post('http://api.pumpup.com/1/functions/feed/profile/load-batch', {
      'isThumbnailsOnly': true,
      'limit': 5,
      'userId': 2707798,
      '_method': 'POST',
      '_version': '5.0.5',
      '_SessionToken':
        [
          'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.',
          'eyJpc3MiOjI3MDc3OTgsImV4cCI6MTUzOTUzNTI1OTM2OH0.',
          'UK2qP1yk9QLk_Bkx1Ly0RPaitRYtec8ojZhzYRc0D-g'
        ].join('')
    })
      .then(({data}) => dispatch({
        type        : RETRIEVE_SLIDER,
        sliderImages: data.result.posts.map(post => post.thumbnail),
      }))

  }

}

export const retrieveGridImages = () => {

  return (dispatch) => {

    return axios.post('http://api.pumpup.com/1/functions/feed/popular/load-batch', {
      'isThumbnailsOnly': true,
      'limit': 18,
      '_method': 'POST',
      '_version': '5.0.5',
      '_SessionToken':
        [
          'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.',
          'eyJpc3MiOjI3MDc3OTgsImV4cCI6MTUzOTUzNTI1OTM2OH0.',
          'UK2qP1yk9QLk_Bkx1Ly0RPaitRYtec8ojZhzYRc0D-g'
        ].join('')
    })
      .then(({data}) => dispatch({
        type      : RETRIEVE_GRID,
        gridImages: data.result.posts.map(post => post.thumbnail),
      }))

  }

}
