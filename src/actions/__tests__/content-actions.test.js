describe('Content Actions', () => {

  let should = global.should
  let sinon = global.sinon
  let proxyquire = global.proxyquire

  let dispatchFunction
  let axiosPostMock
  let ContentActions

  before(() => {
    dispatchFunction = sinon.stub()
    axiosPostMock = sinon.stub().returns(new Promise((resolve, reject) => resolve({
      data: {
        bio: 'MOCK_BIO',
        name: 'MOCK_NAME',
        profileThumbnail: 'MOCK_PROFILE_THUMBNAIL',
      }
    })))

    ContentActions = proxyquire('actions/content-actions', {
      'axios': {
        post: axiosPostMock
      }
    })

  })

  it('should trigger RETRIEVE_PROFILE', () => {


    return ContentActions.retrieveProfile()(dispatchFunction).then(() => {
      should(axiosPostMock).be.calledOnce()
      should(axiosPostMock).be.calledWithMatch('http://api.pumpup.com/1/classes/User/318381', {
        '_method': 'GET',
        '_version': '5.0.5',
        '_SessionToken':
          'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOjI3MDc3OTgsImV4cCI6MTUzOTUzNTI1OTM2OH0.UK2qP1yk9QLk_Bkx1Ly0RPaitRYtec8ojZhzYRc0D-g'
      })

      should(dispatchFunction).be.calledOnce()
      should(dispatchFunction).be.calledWith({
        type: ContentActions.RETRIEVE_PROFILE,
        bio: 'MOCK_BIO',
        name: 'MOCK_NAME',
        profileThumbnail: 'MOCK_PROFILE_THUMBNAIL'
      })
    })
  })

  it('should trigger RETRIEVE_SLIDER', () => {
    axiosPostMock.reset()
    dispatchFunction.reset()
    axiosPostMock.returns(new Promise((resolve, reject) => resolve({
      data: {
        result: {
          posts: [
            {thumbnail: 'MOCK_THUMBNAIL_1'},
            {thumbnail: 'MOCK_THUMBNAIL_2'},
            {thumbnail: 'MOCK_THUMBNAIL_3'},
            {thumbnail: 'MOCK_THUMBNAIL_4'}
          ]
        }
      }
    })))

    return ContentActions.retrieveSliderImages()(dispatchFunction).then(() => {
      should(axiosPostMock).be.calledOnce()
      should(axiosPostMock).be.calledWithMatch('http://api.pumpup.com/1/functions/feed/profile/load-batch', {
        'isThumbnailsOnly': true,
        'limit': 5,
        'userId': 2707798,
        '_method': 'POST',
        '_version': '5.0.5',
        '_SessionToken':
          'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOjI3MDc3OTgsImV4cCI6MTUzOTUzNTI1OTM2OH0.UK2qP1yk9QLk_Bkx1Ly0RPaitRYtec8ojZhzYRc0D-g'
      })

      should(dispatchFunction).be.calledOnce()
      should(dispatchFunction).be.calledWithMatch({
        type: ContentActions.RETRIEVE_SLIDER,
        sliderImages: [
          'MOCK_THUMBNAIL_1',
          'MOCK_THUMBNAIL_2',
          'MOCK_THUMBNAIL_3',
          'MOCK_THUMBNAIL_4'
        ]
      })
    })
  })

  it('should trigger RETRIEVE_GRID', () => {
    axiosPostMock.reset()
    dispatchFunction.reset()
    axiosPostMock.returns(new Promise((resolve, reject) => resolve({
      data: {
        result: {
          posts: [
            {thumbnail: 'MOCK_THUMBNAIL_1'},
            {thumbnail: 'MOCK_THUMBNAIL_2'},
            {thumbnail: 'MOCK_THUMBNAIL_3'},
            {thumbnail: 'MOCK_THUMBNAIL_4'}
          ]
        }
      }
    })))

    return ContentActions.retrieveGridImages()(dispatchFunction).then(() => {
      should(axiosPostMock).be.calledOnce()
      should(axiosPostMock).be.calledWithMatch('http://api.pumpup.com/1/functions/feed/popular/load-batch', {
        'isThumbnailsOnly': true,
        'limit': 18,
        '_method': 'POST',
        '_version': '5.0.5',
        '_SessionToken':
          'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOjI3MDc3OTgsImV4cCI6MTUzOTUzNTI1OTM2OH0.UK2qP1yk9QLk_Bkx1Ly0RPaitRYtec8ojZhzYRc0D-g'
      })

      should(dispatchFunction).be.calledOnce()
      should(dispatchFunction).be.calledWithMatch({
        type: ContentActions.RETRIEVE_GRID,
        gridImages: [
          'MOCK_THUMBNAIL_1',
          'MOCK_THUMBNAIL_2',
          'MOCK_THUMBNAIL_3',
          'MOCK_THUMBNAIL_4'
        ]
      })
    })
  })
})
