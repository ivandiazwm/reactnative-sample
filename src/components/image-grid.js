import React from 'react'
import { Image, StyleSheet, View } from 'react-native'
import PropTypes from 'prop-types'

export default class ImageGrid extends React.Component {

  static propTypes = {
    images: PropTypes.arrayOf(PropTypes.string)
  }

  static defaultProps = {
    images: []
  }



  render() {

    return (
      <View style={styles.imageGrid}>
        {this.props.images.map(this.renderImage.bind(this))}
      </View>
    )

  }



  renderImage(imageURL, index) {

    return (
      <Image key={index} resizeMode='cover' source={{uri: imageURL}} style={styles.image} />
    )
    
  }
}

const styles = StyleSheet.create({
  imageGrid: {
    flex         : 1,
    flexWrap     : 'wrap',
    flexDirection: 'row'
  },
  image: {
    height   : 120,
    flexBasis: 120,
    flex     : 1
  }
})
