import {Text, Image} from 'react-native'
import Profile from '../profile.js'

describe('Profile component', () => {

  let should = global.should
  let shallow = global.shallow
  let React = global.React

  it('should render correctly', () => {

    let profile = shallow(
      <Profile
        imageURL={'MOCK_IMAGE_URL'}
        name={'MOCK_NAME'}
        description={'MOCK_DESCRIPTION @example'}/>
    )

    should(profile.find(Image).prop('source')).be.match({uri: 'MOCK_IMAGE_URL'})
    should(profile.find(Text).at(0).prop('children')).be.equal('MOCK_NAME')
    should(profile.find(Text).at(2).prop('children')).be.equal('MOCK_DESCRIPTION ')
    should(profile.find(Text).at(3).prop('children')).be.match(['@', 'example'])
  })


  it('should collapse long text', () => {

    let profile = shallow(
      <Profile
        imageURL={'MOCK_IMAGE_URL'}
        name={'MOCK_NAME'}
        description={'MOCK_DESCRIPTION \n\n\n MOCK_DESCRIPTION'}/>
    )

    should(profile.find(Text).at(2).prop('children')).be.equal('MOCK_DESCRIPTION \n\n\n')
    should(profile.find(Text).at(3).prop('children')).be.equal('...read more')

    profile.setState({ textCollapsed: false })

    should(profile.find(Text).at(2).prop('children')).be.equal('MOCK_DESCRIPTION \n\n\n MOCK_DESCRIPTION')
    should(profile.find(Text).at(3).prop('children')).be.equal(' read less')
  })

})
