import {Image} from 'react-native'
import ImageGrid from '../image-grid.js'

describe('ImageGrid component', () => {

  let should = global.should
  let shallow = global.shallow
  let React = global.React

  it('should render images', () => {

    let images = [
      'IMAGE_URL_1',
      'IMAGE_URL_2',
      'IMAGE_URL_3',
      'IMAGE_URL_4'
    ]

    let imageGrid = shallow(
      <ImageGrid
        images={images} />
    )

    should(imageGrid.find(Image).length).be.equal(4)
    should(imageGrid.find(Image).at(0).prop('source')).be.match({uri: 'IMAGE_URL_1'})
    should(imageGrid.find(Image).at(1).prop('source')).be.match({uri: 'IMAGE_URL_2'})
    should(imageGrid.find(Image).at(2).prop('source')).be.match({uri: 'IMAGE_URL_3'})
    should(imageGrid.find(Image).at(3).prop('source')).be.match({uri: 'IMAGE_URL_4'})
  })
})
