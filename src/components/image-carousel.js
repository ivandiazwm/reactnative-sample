import React, { Component } from 'react'
import {View, Image, StyleSheet, TouchableOpacity} from 'react-native'
import Swiper from 'react-native-swiper'
import PropTypes from 'prop-types'

export default class ImageCarousel extends Component {

  static propTypes = {
    images: PropTypes.arrayOf(PropTypes.string),
  };

  state = {
    index: 0
  };



  render () {

    return (
      <View>
        <Swiper style={styles.wrapper} height={240}
          showsPagination={false}
          onIndexChanged={index => this.setState({index})}
          loop={false}
          ref="swiper">
          {this.props.images.map(this.renderImageSlide.bind(this))}
        </Swiper>
        <View style={{flex: 1, flexDirection: 'row', justifyContent: 'center'}}>
          {this.props.images.map(this.renderDot.bind(this))}
        </View>
      </View>
    )

  }



  renderImageSlide(url, index) {

    return (
      <View style={styles.slide} key={index}>
        <Image resizeMode='contain' style={styles.image} source={{uri: url}} />
      </View>
    )

  }



  renderDot(url, index) {

    return (
      <TouchableOpacity key={index} onPress={() => {
        this.setState({index}, () => this.refs.swiper.scrollBy(index, true))
      }}>
        <View
          style={(index === this.state.index) ? styles.dot : styles.activeDot} />
      </TouchableOpacity>
    )

  }

}

const styles = StyleSheet.create({
  wrapper: {

  },

  slide: {
    flex           : 1,
    justifyContent : 'center',
    paddingLeft    : 25,
    paddingRight   : 25,
    backgroundColor: 'transparent',
  },

  text: {
    color     : '#fff',
    fontSize  : 30,
    fontWeight: 'bold'
  },

  image: {
    width          : '100%',
    flex           : 1,
    backgroundColor: '#F5F5F5'
  },

  dot: {
    backgroundColor: '#000',
    width          : 11,
    height         : 11,
    borderRadius   : 8,
    marginLeft     : 6,
    marginRight    : 6,
    marginTop      : 15,
    marginBottom   : 30
  },

  activeDot: {
    backgroundColor: 'rgba(0,0,0,.2)',
    width          : 12,
    height         : 12,
    borderRadius   : 8,
    marginLeft     : 6,
    marginRight    : 6,
    marginTop      : 15,
    marginBottom   : 30
  }
})
