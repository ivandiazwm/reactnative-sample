import React from 'react'
import { Image, StyleSheet, Text, View } from 'react-native'
import PropTypes from 'prop-types'
import richTextParser from '../lib/rich-text-parser.js'

export default class Profile extends React.Component {

  static propTypes = {
    imageURL   : PropTypes.string.isRequired,
    name       : PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    maxLines   : PropTypes.number
  }

  static defaultProps = {
    maxLines: 3
  }

  state = {
    textCollapsed: true
  }



  render() {

    return (
      <View style={styles.profileContainer}>
        <View style={styles.profilePicContainer}>
          {(this.props.imageURL) ? <Image source={{uri: this.props.imageURL}} style={styles.profilePic} /> : null}
        </View>
        <View style={styles.profileInfoContainer}>
          <Text style={styles.name}>{this.props.name}</Text>
          <Text style={styles.description}>
            {richTextParser.parse(this.getDescription())}
            {(this.props.description.length > this.getCollapsedDescription().length) ? this.renderTextCollapser() : null}
          </Text>
        </View>
      </View>
    )

  }



  renderTextCollapser() {

    return (
      <Text style={styles.textCollapser} onPress={() => this.setState({textCollapsed: !this.state.textCollapsed})}>
        {this.state.textCollapsed ? '...read more' : ' read less'}
      </Text>
    )

  }



  getDescription() {

    return this.state.textCollapsed ? this.getCollapsedDescription() : this.props.description

  }



  getCollapsedDescription() {

    const CHARACTERS_PER_LINE = 30
    let collapsedString = ''
    let totalChars = 0

    for(let index = 0; index < this.props.description.length; ++index) {
      let character = this.props.description[index]

      if(character === '\n') {totalChars += CHARACTERS_PER_LINE}
      else {totalChars++}

      collapsedString += character

      if(totalChars >= CHARACTERS_PER_LINE*this.props.maxLines) {break}
    }

    return collapsedString

  }
  
}

const PROFILE_PIC_SIZE = 110

const styles = StyleSheet.create({
  profileContainer: {
    padding      : 10,
    flex         : 1,
    flexDirection: 'row'
  },
  profilePicContainer: {
    marginLeft: 10,
    flexBasis : PROFILE_PIC_SIZE,
    alignItems: 'center'
  },
  profileInfoContainer: {
    paddingLeft: 20,
    flexBasis  : 200,
    flexGrow   : 1,
  },
  profilePic: {
    width       : PROFILE_PIC_SIZE,
    height      : PROFILE_PIC_SIZE,
    borderRadius: PROFILE_PIC_SIZE
  },
  textCollapser: {
    color: '#CCCCCC'
  },
  name: {
    marginTop: 10,
    fontSize : 19
  },
  description: {
    marginTop: 10,
    fontSize : 12,
    color    : '#666666'
  }
})
