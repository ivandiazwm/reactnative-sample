import React from 'react'
import PropTypes from 'prop-types'
import { ScrollView, View, StyleSheet } from 'react-native'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import * as Actions from './actions/content-actions.js'
import Profile from './components/profile.js'
import ImageCarousel from './components/image-carousel.js'
import ImageGrid from './components/image-grid.js'



class App extends React.Component {

  static propTypes = {
    actions: PropTypes.object.isRequired,
    content: PropTypes.object.isRequired
  }

  state = {
    imageIndex: 0
  };



  componentDidMount() {
    this.props.actions.retrieveProfile()
    this.props.actions.retrieveSliderImages()
    this.props.actions.retrieveGridImages()
  }



  render() {

    return (
      <ScrollView style={styles.container}>
        <Profile
          imageURL={this.props.content.profileThumbnail}
          name={this.props.content.name}
          description={this.props.content.bio} />
        <View style={styles.separator}/>
        <ImageCarousel
          index={this.state.imageIndex}
          onIndexChanged={imageIndex => this.setState({imageIndex})}
          images={this.props.content.sliderImages} />
        <ImageGrid images={this.props.content.gridImages} />
      </ScrollView>
    )

  }

}

const mapStateToProps = state => ({
  content: state.content
})

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(Actions, dispatch)
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App)

const styles = StyleSheet.create({
  container: {
    marginTop      : 25,
    backgroundColor: '#fff',
    flex           : 1,
  },
  separator: {
    marginTop      : 20,
    marginBottom   : 20,
    height         : 2,
    backgroundColor: '#CCCCCC',
  }
})
