import React from 'react'
import { createStore, applyMiddleware } from 'redux'
import { Provider } from 'react-redux'
import App from './src/app.js'
import reducer from './src/reducers'
import thunk from 'redux-thunk'

export default () => (
  <Provider store={store}>
    <App />
  </Provider>
)

const store = createStore(
  reducer,
  applyMiddleware(thunk)
)
